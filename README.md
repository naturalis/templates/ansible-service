# ansible-project

Infrastructure configuration of _servicename_ using Ansible on _cloudservice_

# Service information

This service is used by _team_ and does _description_

Contact information: _serviceowner_

# Design decision

- _exception1_, because _reason1_
- _exception2_, because _reason2_

# Links

- _Documentation if available_
- _Other gitlab repos_ (e.g. Terraform repo)

# License

Apache2
